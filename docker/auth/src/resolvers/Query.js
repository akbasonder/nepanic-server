const { APP_SECRET,ENVIRONMENT,USER_ID } = require('../constants')
const { sign } = require('jsonwebtoken')

const Query = {
  getToken: async (parent, { userId }, ctx) => {
    //const id = decrypt(userId);
    const id= userId;
    if(id!==USER_ID){
      return {
        token: 'INVALID USER_ID',    
      }
    }
    const token = sign({ userId: id }, APP_SECRET);
      return {
        //token: encrypt(token),    
        token
      }
  },
}


module.exports = {
  Query,
}