/*const { hash, compare } = require('bcrypt')
const { sign } = require('jsonwebtoken')
const { encrypt,decrypt } = require('../utils')
const { APP_SECRET, ENVIRONMENT } = require('../constants')

var logger = require('loglevel');
logger.setLevel(ENVIRONMENT > 1?'warn':'debug');*/
const { PASSWORD } = require('../constants')
var generateSafeId = require('generate-safe-id');

const Mutation = {

  async updateContents(root, args, context) {
    //console.log('context is',context);
    if (args.password !== PASSWORD) {
      throw new Error(`Incorrect password`);
    }
    const { fromMinVersion, fromStatus, toVersion, toStatus } = args;
    if (!fromMinVersion) {
      throw new Error(`fromMinVersion is necessary`);
    }
    await context.db.updateManyCategories({ where: { minVersion: fromMinVersion, status: fromStatus }, data: { minVersion: toVersion, status: toStatus } });
    return { id: 1 }
  },
  /*updateManyCategories(root, args, context) {
    //console.log('context is',context);
    if(args.password!==PASSWORD){
      throw new Error(`Incorrect password`);
    }
    return context.db.updateManyCategories({ where: args.where, data: args.data });
  },

  /*deleteManyCategories(root, args, context) {
    //console.log('context is',context);
    return context.db.deleteManyCategories({ where: args.where, data: args.data });    
  },*/

  /*insertContent: async (parent, { name, parentName, imageURL_tr, imageURL_en, minVersion }, ctx) => {
    if (!minVersion)
      return;
    const parentCatogery = await ctx.db.category({ name: parentName });
    //console.log('parentCatogery',parentCatogery);
    if (!parentCatogery.hasChildren) {
      await ctx.db.updateCategory({ where: { name: parentName }, data: { hasChildren: true } });
    }
    const maxRankRecordsTR = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })
    //console.log('maxRankRecordsTR',maxRankRecordsTR);
    //const maxRankEN = await ctx.db.categoryInfoes({where:{category:{parent:{ name:"Panik Atak"}},languageCode:"en"}, orderBy:rank_DESC,  first:1})
    const maxRankTR = maxRankRecordsTR[0].rank + 1;
    console.log('maxRankTR', maxRankTR);

    const category = await ctx.db.createCategory({
      name,
      parent: { connect: { name: parentName } },
      columnCount: 1,
      hasChildren: false,
      minVersion
    });
    //console.log('created Category',category);

    const categoryInfoTR = await ctx.db.createCategoryInfo({
      languageCode: "tr",
      imageURL: imageURL_tr,
      rank: maxRankTR,
      category: { connect: { id: category.id } }
    });

    const categoryInfoEN = await ctx.db.createCategoryInfo({
      languageCode: "en",
      imageURL: imageURL_en,
      rank: maxRankTR,
      category: { connect: { id: category.id } }
    });

    //console.log('categoryInfoEN',categoryInfoEN);

    // TODO: user.id won't returned in result
    return {
      //id: category.id
      id:1
    }
  },*/

  insertContents: async (parent, { password, parentName, contents, minVersion }, ctx) => {
    //console.log('contents',contents);
    //console.log('contents[0]',contents[0]);

    if (password !== PASSWORD) {
      throw new Error(`Incorrect password`)
    }

    const maxRankRecords = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })
    
    let maxRank = maxRankRecords[0].rank;
    //console.log('maxRank=', maxRank);
    for (let content of contents) {
      maxRank++;
      //console.log('content====', content)
      var contentName = generateSafeId();
      const category1 = await ctx.db.createCategory({
        name: contentName,
        parent: { connect: { name: parentName } },
        columnCount: 1,
        hasChildren: true,
        minVersion
      });
      //console.log('category1 created', category1);

      const categoryInfo11 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.titleURL_tr,
        rank: maxRank,
        category: { connect: { id: category1.id } }
      });
      //console.log('categoryInfo11 created', categoryInfo11);

      const categoryInfo12 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.titleURL_en,
        rank: maxRank,
        category: { connect: { id: category1.id } }
      });
      //console.log('categoryInfo12 created', categoryInfo12);

      const category2 = await ctx.db.createCategory({
        name: contentName + ' content',
        parent: { connect: { name: contentName } },
        columnCount: 1,
        hasChildren: false,
        minVersion
      });
      //console.log('category1 created', category1);

      const categoryInfo21 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.contentURL_tr,
        rank: 1,
        category: { connect: { id: category2.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo22 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.contentURL_en,
        rank: 1,
        category: { connect: { id: category2.id } }
      });
      //console.log('categoryInfo22 created', categoryInfo22);

    }
    return { id: 1 }

  },


  insertDoubleContents: async (parent, { password, parentName, contents, minVersion }, ctx) => {

    if (password !== PASSWORD) {
      throw new Error(`Incorrect password`)
    }

    const maxRankRecords = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })

    let maxRank = maxRankRecords[0].rank;
    //console.log('maxRank=', maxRank);
    for (let content of contents) {
      const firstCategoryInfo = await ctx.db.categoryInfoes({ where: {  category: { parent: { name: parentName, status: "active" }},imageURL:content.titleURL_tr, languageCode: "tr" } })
      .$fragment('fragment configWithUser on CategoryInfo { category {name} }');
      
      //console.log('content====', content)
      let contentName = generateSafeId();
      if(!firstCategoryInfo || firstCategoryInfo.length===0){
        //console.log('firstCategoryInfo has value');
        maxRank++;
        const category1 = await ctx.db.createCategory({
          name: contentName,
          parent: { connect: { name: parentName } },
          columnCount: 1,
          hasChildren: true,
          minVersion
        });
        //console.log('category1 created', category1);

        const categoryInfo11 = await ctx.db.createCategoryInfo({
          languageCode: "tr",
          imageURL: content.titleURL_tr,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });
        //console.log('categoryInfo11 created', categoryInfo11);
  
        const categoryInfo12 = await ctx.db.createCategoryInfo({
          languageCode: "en",
          imageURL: content.titleURL_en,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });
        //console.log('categoryInfo12 created', categoryInfo12);
      }else{
        //console.log('firstCategoryInfo2',firstCategoryInfo[0]);
        contentName = firstCategoryInfo[0].category.name;
        
      }      
      
      //console.log('categoryInfo12 created', categoryInfo12);

      const secondContentName = generateSafeId();//contentName + ' content';
      //console.log('secondContentName',secondContentName);
      const category2 = await ctx.db.createCategory({
        name: secondContentName,
        parent: { connect: { name: contentName } },
        columnCount: 1,
        hasChildren: true,
        minVersion
      });
      //console.log('category2 created', category2);

      const maxRankRecords2 = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 });
      let maxRank2 = maxRankRecords2[0].rank;
      //console.log('maxRank2',maxRank2);

      const categoryInfo21 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.secondTitleURL_tr,
        rank: maxRank2+1,
        category: { connect: { id: category2.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo22 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.secondTitleURL_en,
        rank: maxRank2+1,
        category: { connect: { id: category2.id } }
      });



      const category3 = await ctx.db.createCategory({
        name: secondContentName + '2',
        parent: { connect: { name: secondContentName } },
        columnCount: 1,
        hasChildren: false,
        minVersion
      });
      //console.log('category3 created', category3);

      const categoryInfo31 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.contentURL_tr,
        rank: 1,
        category: { connect: { id: category3.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo32 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.contentURL_en,
        rank: 1,
        category: { connect: { id: category3.id } }
      });

      //console.log('categoryInfo22 created', categoryInfo22);

    }
    return { id: 1 }

  },


  insertTherapies: async (parent, { password, contents, minVersion }, ctx) => {

    if (password !== PASSWORD) {
      throw new Error(`Incorrect password`)
    }
    const parentName = "Terapiler";

    const maxRankRecords = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })

    let maxRank = maxRankRecords[0].rank;
    //console.log('maxRank=', maxRank);
    for (let content of contents) {
      const firstCategoryInfo = await ctx.db.categoryInfoes({ where: {  category: { parent: { name: parentName, status: "active" }},imageURL:content.titleURL_tr, languageCode: "tr" } })
      .$fragment('fragment configWithUser on CategoryInfo { category {name} }');
      
      //console.log('content====', content)
      let contentName = generateSafeId();
      if(!firstCategoryInfo || firstCategoryInfo.length===0){
        maxRank++;
        const category1 = await ctx.db.createCategory({
          name: contentName,
          parent: { connect: { name: parentName } },
          columnCount: 1,
          hasChildren: true,
          minVersion
        });

        const categoryInfo11 = await ctx.db.createCategoryInfo({
          languageCode: "tr",
          imageURL: content.titleURL_tr,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });
        
  
        const categoryInfo12 = await ctx.db.createCategoryInfo({
          languageCode: "en",
          imageURL: content.titleURL_en,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });        
      }else{        
        contentName = firstCategoryInfo[0].category.name;
      }      
      
      //console.log('categoryInfo12 created', categoryInfo12);

      const secondContentName = generateSafeId();//contentName + ' content';
      const category2 = await ctx.db.createCategory({
        name: secondContentName,
        parent: { connect: { name: contentName } },
        columnCount: 3,
        rowHeight:90,
        hasChildren: true,
        minVersion
      });
      

      //const maxRankRecords2 = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 });
      //let maxRank2 = maxRankRecords2[0].rank;
      //console.log('maxRank2',maxRank2);

      const categoryInfo21 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        content: content.contentNumber>10?content.contentNumber.toString():'0'+content.contentNumber,
        rank: content.contentNumber,
        category: { connect: { id: category2.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo22 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        content: content.contentNumber>10?content.contentNumber.toString():'0'+content.contentNumber,
        rank: content.contentNumber,
        category: { connect: { id: category2.id } }
      });



      const category3 = await ctx.db.createCategory({
        name: secondContentName + '2',
        parent: { connect: { name: secondContentName } },
        columnCount: 1,
        hasChildren: false,
        minVersion
      });
      
      const categoryInfo31 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.contentURL_tr,
        rank: 1,
        category: { connect: { id: category3.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo32 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.contentURL_en,
        rank: 1,
        category: { connect: { id: category3.id } }
      });

      //console.log('categoryInfo22 created', categoryInfo22);

    }
    return { id: 1 }

  },


  /*insertTherapies: async (parent, { password, contents, minVersion }, ctx) => {

    if (password !== PASSWORD) {
      throw new Error(`Incorrect password`)
    }
    const parentName = "Terapiler";

    const maxRankRecords = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })

    let maxRank = maxRankRecords[0].rank;
    //console.log('maxRank=', maxRank);
    for (let content of contents) {
      maxRank++;
      //console.log('content====', content)
      var contentName = generateSafeId();
      const category1 = await ctx.db.createCategory({
        name: contentName,
        parent: { connect: { name: parentName } },
        columnCount: 1,
        hasChildren: true,
        minVersion
      });
      //console.log('category1 created', category1);

      const categoryInfo11 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.titleURL_tr,
        rank: maxRank,
        category: { connect: { id: category1.id } }
      });
      //console.log('categoryInfo11 created', categoryInfo11);

      const categoryInfo12 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.titleURL_en,
        rank: maxRank,
        category: { connect: { id: category1.id } }
      });
      //console.log('categoryInfo12 created', categoryInfo12);

      const secondContentName = contentName + ' content';
      const category2 = await ctx.db.createCategory({
        name: secondContentName,
        parent: { connect: { name: contentName } },
        columnCount: content.secondTitleNumber ? 3 : 1,
        hasChildren: true,
        minVersion
      });
      //console.log('category1 created', category1);

      const categoryInfo21 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.secondTitleURL_tr,
        content: content.secondTitleNumber,
        rank: content.secondTitleNumber ? parseInt(content.secondTitleNumber) : 1,
        category: { connect: { id: category2.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo22 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.secondTitleURL_en,
        content: content.secondTitleNumber,
        rank: content.secondTitleNumber ? parseInt(content.secondTitleNumber) : 1,
        category: { connect: { id: category2.id } }
      });


      const category3 = await ctx.db.createCategory({
        name: secondContentName + '2',
        parent: { connect: { name: secondContentName } },
        columnCount: 1,
        hasChildren: false,
        minVersion
      });
      //console.log('category1 created', category1);

      const categoryInfo31 = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: content.contentURL_tr,
        rank: 1,
        category: { connect: { id: category3.id } }
      });
      //console.log('categoryInfo21 created', categoryInfo21);

      const categoryInfo32 = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: content.contentURL_en,
        rank: 1,
        category: { connect: { id: category3.id } }
      });

      //console.log('categoryInfo22 created', categoryInfo22);

    }
    return { id: 1 }

  },


  /*insertMultipleTherapies: async (parent, {password, parentName, contents, minVersion }, ctx) => {
    //console.log('contents',contents);
    //console.log('contents[0]',contents[0]);
    try {
      if(password!==PASSWORD){
        throw new Error(`Incorrect password`)
      }
        
      const maxRankRecords = await ctx.db.categoryInfoes({ where: { category: { parent: { name: parentName, status: "active" } }, languageCode: "tr" }, orderBy: 'rank_DESC', first: 1 })

      let maxRank = maxRankRecords[0].rank;
      //console.log('maxRank=', maxRank);
      for (let content of contents) {
        maxRank ++;
        //console.log('content====', content)
        var contentName = generateSafeId();
        const category1 = await ctx.db.createCategory({
          name: contentName,
          parent: { connect: { name: parentName } },
          columnCount: 1,
          hasChildren: true,
          minVersion
        });
        //console.log('category1 created', category1);

        const categoryInfo11 = await ctx.db.createCategoryInfo({
          languageCode: "tr",
          imageURL: content.titleURL_tr,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });
        //console.log('categoryInfo11 created', categoryInfo11);

        const categoryInfo12 = await ctx.db.createCategoryInfo({
          languageCode: "en",
          imageURL: content.titleURL_en,
          rank: maxRank,
          category: { connect: { id: category1.id } }
        });
        //console.log('categoryInfo12 created', categoryInfo12);

        const category2 = await ctx.db.createCategory({
          name: contentName + ' content',
          parent: { connect: { name: contentName } },
          columnCount: 1,
          hasChildren: false,
          minVersion
        });
        //console.log('category1 created', category1);

        const categoryInfo21 = await ctx.db.createCategoryInfo({
          languageCode: "tr",
          imageURL: content.contentURL_tr,
          rank: 1,
          category: { connect: { id: category2.id } }
        });
        //console.log('categoryInfo21 created', categoryInfo21);

        const categoryInfo22 = await ctx.db.createCategoryInfo({
          languageCode: "en",
          imageURL: content.contentURL_en,
          rank: 1,
          category: { connect: { id: category2.id } }
        });
        //console.log('categoryInfo22 created', categoryInfo22);

      }
      return { id: 1 }
    }catch(err){
      console.log('insert category error is',err);
      return { id: 0 }
    }
  },

  /*insertTherapy : async (parent, { name, parentName, imageURL_tr, imageURL_en, version }, ctx) => {    
    const category = await ctx.db.createCategory({
      name,
      parent:{connect:{name:parentName}},
      coulmnCount,
      hasChildren,      
    });
  },*/



  /*createCategoryWithData: async (parent, { name, parentName, columnCount, hasChildren, imageURL_tr, content_tr, imageURL_en, content_en, rank }, ctx) => {
    //const parent2 = parentName?'{connect:{name:parentName}}':undefined; 
    const category = await ctx.db.createCategory({
      name,
      parent: { connect: { name: parentName } },
      columnCount,
      hasChildren,
    });

    //console.log('category',category);

    if (imageURL_tr || content_tr) {
      const categoryInfoTR = await ctx.db.createCategoryInfo({
        languageCode: "tr",
        imageURL: imageURL_tr,
        content: content_tr,
        rank,
        category: { connect: { id: category.id } }
      });
    }


    //console.log('categoryInfoTR',categoryInfoTR);
    if (imageURL_en || content_en) {
      const categoryInfoEN = await ctx.db.createCategoryInfo({
        languageCode: "en",
        imageURL: imageURL_en,
        content: content_en,
        rank,
        category: { connect: { id: category.id } }
      });
    }

    //console.log('categoryInfoEN',categoryInfoEN);

    // TODO: user.id won't returned in result
    return {
      id: category.id
    }

  },*/

}

module.exports = {
  Mutation,
}
