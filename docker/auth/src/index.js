//const { GraphQLServer } = require('graphql-yoga')
const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./generated/prisma-client')
const { resolvers } = require('./resolvers')
const {ENDPOINT,PORT,SSL_CERT_PATH,SSL_KEY_PATH} = require('./constants');
const fs = require('fs');


const options = {
  port: PORT,
  //endpoint: '/waid/dev',
}

const server = new GraphQLServer({
  typeDefs: 'src/schema.graphql',
  resolvers,
  context: req => {
    return {
      ...req,
      db: prisma,
    }
  },
})

//server.start(() => console.log('Server is running on http://localhost:4000'))

server.start({
  ...options,
  https: {
    key: fs.readFileSync(SSL_KEY_PATH,'utf8'),
    cert: fs.readFileSync(SSL_CERT_PATH,'utf8')
  }
});
