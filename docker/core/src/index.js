const { prisma } = require('./generated/prisma-client')
const { GraphQLServer } = require('graphql-yoga')
const jwt = require("jsonwebtoken");
const { AuthenticationError } = require("apollo-server-core");
const fs = require('fs');
const { ENDPOINT, PORT, SSL_CERT_PATH, SSL_KEY_PATH, APP_SECRET,ENVIRONMENT } = require('./constants');


const resolvers = {

  Query: {
    authPayload(root, args, context) {
      return context.prisma.authPayload(args.where)
    },
    authPayloads(root, args, context) {
      return context.prisma.authPayloads(args)
    },
    device(root, args, context) {
      return context.prisma.device(args.where)
    },
    devices(root, args, context) {
      return context.prisma.devices(args)
    },
    user(root, args, context) {
      return context.prisma.user(args.where);
    },
    usersConnection(root, args, context) {
      const { prisma } = context;
      const newReturn = {
        pageInfo: prisma.usersConnection(args).pageInfo(),
        edges: prisma.usersConnection(args).edges(),
        aggregate: prisma.usersConnection(args).aggregate()
      }
      return newReturn;
    },    
    users(root, args, context) {
      return context.prisma.users(args)
    },
    payment(root, args, context) {
      return context.prisma.payment(args.where);
    },
    payments(root, args, context) {
      return context.prisma.payments(args)
    },
    category(root, args, context) {
      return context.prisma.category(args.where);
    },
    categories(root, args, context) {
      return context.prisma.categories(args)
    },
    categoryInfo(root, args, context) {
      return context.prisma.categoryInfo(args.where);
    },
    //categoryInfoes(root, args, context) {
    //  return context.prisma.categoryInfoes(args)
    //},
    diary(root, args, context) {
      return context.prisma.diary(args.where);
    },
    diaries(root, args, context) {
      return context.prisma.diaries(args).
      $fragment('fragment diariesWithUser on Diary {' +
      ' id createdAt updatedAt content title status ' +
      ' user { id  } }' 
      );
    },
    categoryInfoes(root, args, context) {
      return context.prisma.categoryInfoes(args).
      $fragment('fragment categoryWithInfoes on CategoryInfoes {' +
      ' id createdAt updatedAt content imageURL languageCode rank' +
      ' category { id createdAt name rowHeight hasChildren columnCount navigateTo } }' 
      );
    },
    /*level(root, args, context) {
      return context.prisma.level(args.where).
      $fragment('fragment categoryWithInfoes on Activity {' +
      ' id levelNumber initialBoard initialBoardProperties maxMove maxRandom minRandom maxDifferent randomStrategy targetPoint minBuild leftSpaceRatio upSpaceRatio maxFruitWiper fruitWiperPoint' +
      ' targetList { id aim color property } }' 
      );
    },
    levels(root, args, context) {
      return context.prisma.levels(args).
      $fragment('fragment categor on Activity {' +
      ' id levelNumber initialBoard initialBoardProperties maxMove maxRandom minRandom maxDifferent randomStrategy targetPoint minBuild leftSpaceRatio upSpaceRatio maxFruitWiper fruitWiperPoint' +
      ' targetList { id aim color property } }' 
      );
      
    },*/
    
  },

  Mutation: {   
    createDevice(root, args, context) {
      return context.prisma.createDeviceInfo(args.data);
    },       
    createUser(root, args, context) {
      return context.prisma.createUser(args.data);
    },
    updateUser(root, args, context) {
      return context.prisma.updateUser({ where: args.where, data: args.data })     
    },
    createPayment(root, args, context) {
      //console.log('createPoint started with args',args);
      return context.prisma.createPayment(args.data);
    },   
    createDiary(root, args, context) {
      return context.prisma.createDiary(args.data);
    },
    updateDiary(root, args, context) {
      return context.prisma.updateDiary({ where: args.where, data: args.data })     
    },
    createUserLog(root, args, context) {
      return context.prisma.createUserLog(args.data);
    },    
  },
  
}


const autheticate = async (resolve, root, args, context, info) => {  
  try {    
    const auth = context.connection ? context.connection.context.Authorization:context.request.get("Authorization");
    const token = jwt.verify(auth.substring(7), APP_SECRET); // we need to get rid of "Bearer "
    //console.log('token=',token);
  } catch (e) {
    return new AuthenticationError(e.message);    
  }
  const result = await resolve(root, args, context, info);
  return result;
};

const options = {
  port: PORT,
  endpoint:  ENDPOINT,//'/waid/dev',
  //secret: `sD83_hfM72_9f7LnsK3ax`
  //subscriptions: ENDPOINT,//'/waid/dev',
  playground: ENDPOINT,//'/playground',
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  //context: {
  //  prisma,
  //},
  context: req => ({ prisma, ...req }),
  middlewares: [autheticate],

})
/*server.start((
options,({port}) => console.log('Server is running on http://localhost:8000')
  
) )*/

/*  server.start(options, ({ port }) =>
  console.log(
    `Server started, listening on port ${port} for incoming requests. https: ${https}`,
  ),
)*/

server.start(ENVIRONMENT===2? {
  ...options,
  https: {
    key: fs.readFileSync(SSL_KEY_PATH),
    cert: fs.readFileSync(SSL_CERT_PATH)
  }
}:{...options});